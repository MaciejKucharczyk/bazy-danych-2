from src.database import Database


def get_wina(db: Database) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'wina'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id, nazwa, kraj, slodkosc, gatunek, data, zdjecie), długość listy
    """

    wina = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.rodzaje_wina;"
    )
    wina_list = []
    for wino in wina:
        id = wino[0]
        nazwa = wino[1]
        kraj = wino[2]
        slodkosc = wino[3]
        gatunek = wino[4]
        data = wino[5]
        zdjecie = wino[6]

        wina_list.append((id, nazwa, kraj, slodkosc, gatunek, data, zdjecie))
    return (wina_list, len(wina_list))

def get_kraje(db: Database) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'kraje'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id, nazwa kraju), długość listy
    """

    kraje = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.kraje;"
    )
    kraje_list = []
    for k in kraje:
        kraje_list.append([k[0], k[1]]) # id, nazwa kraju

    return (kraje_list, len(kraje_list))


def get_gatunki(db: Database) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'gatunki'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id, gatunek), długość listy
    """

    gatunki = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.gatunki;"
    )
    gatunki_list = []
    for g in gatunki:
        gatunki_list.append([g[0], g[1]])
    return (gatunki_list, len(gatunki_list))


def get_slodkosc(db: Database) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'slodkosc'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id, słodkość), długość listy
    """

    slodkosci = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.slodkosc;"
    )
    slodkosc_list = []
    for s in slodkosci:
        slodkosc_list.append([s[0], s[1]])
    return (slodkosc_list, len(slodkosc_list))

def get_slodkosc(db: Database) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'slodkosc'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id, słodkość), długość listy
    """

    slodkosci = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.slodkosc;"
    )
    slodkosc_list = []
    for s in slodkosci:
        slodkosc_list.append([s[0], s[1]])
    return (slodkosc_list, len(slodkosc_list))

def get_name_from_id(id: int, wina: tuple[list, int]) -> str:
    """Funkcja zwracająca nazwę wina o podanym id

    Args:
        id (int): id wina
        wina (tuple[list, int]): dane wyciągnięte z tabeli 'rodzaje_wina'

    Returns:
        str: nazwa wina
    """

    for wino in wina[0]:
        if id == wino[0]:
            return wino[1]
    return ""

def get_magazyn(db: Database, wina: tuple[list, int]) -> tuple[list, int]:
    """Wyciąga dane z tabeli 'magazyn'

    Args:
        db (Database): baza danych z której mają być wyciągnięte informacje

    Returns:
        tuple[list, int]: list(id_wina, nazwa, ilosc, nr_regalu, nr_partii), długość listy
    """

    magazyn = db.select_from_database(
        "SELECT * FROM jstasik_bazy_danych_2.magazyn ORDER BY id_wina;"
    )
    magazyn_list = []
    for s in magazyn:
        id_wina = s[1]
        nazwa = get_name_from_id(id_wina, wina)
        ilosc = s[2]
        nr_regalu = s[0]
        nr_partii = s[3]
        magazyn_list.append((id_wina, nazwa, ilosc, nr_regalu, nr_partii))

    return (magazyn_list, len(magazyn_list))