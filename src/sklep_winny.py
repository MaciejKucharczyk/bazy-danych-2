from flask import Flask, render_template, request, url_for, redirect
from src.store_data import get_wina, get_gatunki, get_kraje, get_slodkosc, get_magazyn
from src.config import USER, PASS, HOST, PORT, DB_NAME
from src.database import Database
import logging
from src.setup_logging import setup_logging

setup_logging()
logger = logging.getLogger(__name__)


target_database = Database(USER, PASS, HOST, PORT, DB_NAME)
app = Flask(__name__)

# strona główna
@app.route("/", methods=["POST", "GET"])
def index():

    # Selecting data from database
    wina = get_wina(target_database)
    kraje = get_kraje(target_database)
    gatunki = get_gatunki(target_database)
    slodkosc = get_slodkosc(target_database)

    # INSERT
    if request.method == "POST":
        if request.form["btn"] == "details":
            # TODO akcja po kliknięciu w produkt, np: wyświetlenie ceny, stanu na magazynie itp.
            return redirect("/")
    else:
        return render_template(
            "index.html",
            wina=wina,
            kraje=kraje,
            gatunki=gatunki,
            slodkosc=slodkosc,
        )

# zarządzanie magazynem
@app.route("/magazyn", methods=["POST", "GET"])
def magazyn():

    # Selecting data from database
    wina = get_wina(target_database)
    kraje = get_kraje(target_database)
    gatunki = get_gatunki(target_database)
    slodkosc = get_slodkosc(target_database)
    magazyn = get_magazyn(target_database, wina)

    # forms handling
    if request.method == "POST":
        # inserting a new type of wine into the database
        if request.form["btn"] == "Dodaj":
            nazwa_wina = request.form["nazwa_wina"]
            kraj = request.form["kraj"]
            slodkosc = request.form["slodkosc"]
            gatunek = request.form["gatunek"]
            target_database.insert_into_database(
                "INSERT INTO jstasik_bazy_danych_2.rodzaje_wina (`nazwa`, `kraj`, `gatunek`, `slodkosc`) VALUES (%s, %s, %s, %s);",
                [nazwa_wina, kraj, slodkosc, gatunek],
            )

        # deleting a wine type from the database
        if request.form["btn"] == "Usuń wino":
            id_wina = request.form["id_wina"] 

            logger.info(f"deleting a wine. wine_id: {id_wina}")
            target_database.delete_from_database("DELETE FROM jstasik_bazy_danych_2.rodzaje_wina WHERE `id_wina`=%s", [id_wina])

        # adding a wine delivery to the warehouse
        if request.form["btn"] == "Dodaj dostawę":
            id_wina = request.form["id_wina"]
            regal = request.form["regal"]
            ilosc = request.form["ilosc"]
            partia = request.form["partia"]
            target_database.insert_into_database(
                "INSERT INTO jstasik_bazy_danych_2.magazyn (`nr_regalu`, `id_wina`, `ilosc`, `nr_partii`) VALUES (%s, %s, %s, %s);",
                [regal, id_wina, ilosc, partia],
            )

        # removal of the entire batch from the warehouse
        if request.form["btn"] == "Wycofaj":
            nr_partii = request.form["nr_partii"]
            logger.info(f"deleting a batch of wine. batch_id: {nr_partii}")
            target_database.delete_from_database("DELETE FROM jstasik_bazy_danych_2.magazyn WHERE `nr_partii`=%s", [nr_partii])

            
        return redirect("/magazyn")
        
    else:
        return render_template(
            "magazyn.html",
            wina=wina,
            kraje=kraje,
            gatunki=gatunki,
            slodkosc=slodkosc,
            magazyn=magazyn
        )

# zarządzanie transakcjami
@app.route("/transakcje", methods=["POST", "GET"])
def transakcje():

    if request.method == "POST":
        if request.form["btn"] == "details":
            # TODO 
            return redirect("/transakcje")
    else:
        return render_template(
            "transakcje.html",
        )


if __name__ == "__main__":
    # python -m src.sklep_winny
    app.run(debug=False)
